package com.zenika.emarket.emarket.factory;

import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.factory.CategoryFactory;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductFactory {


    public static Product peche(){

        return new Product("peche", 2, CategoryFactory.fruits() , "none");
    }

    public static Product audi(){
        return new Product("audi", 20000, CategoryFactory.voitures(), "none");

    }

    public static Product fraise(){
        return new Product("fraise", 4, CategoryFactory.fruits(), "none");
    }
}
