package com.zenika.emarket.emarket.domain;


import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.error.ProductNullException;
import com.zenika.emarket.emarket.factory.ProductFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ProductTest {




    @Test
    public void updateProduct() {
        Product fraise = ProductFactory.fraise();

        fraise.setProductPrice(2);

        Assertions.assertThat(fraise.getProductPrice())
                .isEqualTo(2);



        Assertions.assertThatThrownBy(() -> fraise.setProductPrice(0))
               .isInstanceOf(ProductNullException.class);





    }

}