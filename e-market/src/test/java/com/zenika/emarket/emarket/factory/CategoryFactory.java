package com.zenika.emarket.emarket.factory;

import com.zenika.emarket.emarket.domain.Categorie;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CategoryFactory {

    public static Categorie fruits() {
        return new Categorie(1, "fruits");
    }


    public static Categorie voitures() {
        return new Categorie(2, "voitures");
    }
}
