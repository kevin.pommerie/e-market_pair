package com.zenika.emarket.emarket.domain;


import com.zenika.emarket.emarket.factory.ProductFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class BasketTest {



    @Test
    public void addProductTest() {
        Basket basket = new Basket();
        Product peche = ProductFactory.peche();

        basket.addProduct(peche, 3);

        Assertions.assertThat(basket.getProductLines()).hasSize(1);

        ProductLine productLine = basket.getProductLines().get(0);
        Assertions.assertThat(productLine.getProduct().getProductName()).isEqualTo("peche");
        Assertions.assertThat(productLine.getQuantity()).isEqualTo(3);

        basket.addProduct(peche, 3);

        Assertions.assertThat(basket.getProductLines()).hasSize(1);

        productLine = basket.getProductLines().get(0);
        Assertions.assertThat(productLine.getProduct().getProductName()).isEqualTo("peche");
        Assertions.assertThat(productLine.getQuantity()).isEqualTo(6);



    }

    @Test
    public  void removeProductTest(){
        Basket basket = new Basket();
        Product audi = ProductFactory.audi();

        basket.addProduct(audi, 3);
        basket.removeProduct(audi, 2);

        Assertions.assertThat(basket.getProductLines())
                .containsOnlyOnce(basket.getProductLineFromProduct(audi));

        Assertions.assertThat(basket.getProductLineFromProduct(audi).getQuantity())
                .isEqualTo(1);

    }

    @Test
    public void removeAllProducts(){
        Basket basket = new Basket();
        Product fraise = ProductFactory.fraise();

        basket.addProduct(fraise, 3);
        basket.removeProduct(fraise, 3);

        Assertions.assertThat(basket.getProductLines()) .hasSize(0);
    }


}