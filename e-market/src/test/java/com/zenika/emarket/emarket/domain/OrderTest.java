package com.zenika.emarket.emarket.domain;

import com.zenika.emarket.emarket.error.ProductNotFoundException;
import com.zenika.emarket.emarket.factory.ProductFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class OrderTest {

    @Test
    public void orderBasket(){

        Basket basket = new Basket();
        Product fraise = ProductFactory.fraise();
        basket.addProduct(fraise, 2);

        Assertions.assertThat(basket.getProductLines()).hasSize(1);

        Order order = OrderFactory.createOrder(basket);


        Assertions.assertThat(order.getOrderLines())
                .hasSize(1)
                .containsOnlyOnce(order.getOrderLineFromProduct(fraise));

        Assertions.assertThat(order.getOrderLines().get(0).getQuantity()).isEqualTo(2);

        fraise.setProductPrice(10);
        // test the product price in the orderline compare to the actual product price.
        // orderline's product price should not change.

        Assertions.assertThat(order.getOrderLines().get(0).getProductPrice()).isEqualTo(4);

        Assertions.assertThat(order.getTotalPrice()).isEqualTo(8);

    }

    @Test
    public void orderEmptyBasket(){

        Basket basket = new Basket();

        Assertions.assertThatThrownBy(() -> OrderFactory.createOrder(basket))
                .isInstanceOf(ProductNotFoundException.class);
    }

}