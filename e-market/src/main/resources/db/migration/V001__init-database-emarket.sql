create table if not exists categories

(
    categorie_id integer not null generated always as identity primary key ,
    categorie_name text not null

);

CREATE TABLE  if not exists product
(
    product_name  text    NOT NULL unique primary key ,
    product_price integer NOT NULL,
    categorie_id integer NOT NULL references categories(categorie_id),
    url_img text

);

CREATE TABLE if not exists basket

(
    basket_id integer generated always as identity primary key ,
    created_at timestamp not null default now()

);


create table if not exists productLine

(
    basket_id integer not null references basket(basket_id),
    product_id text not null references product(product_name),
    quantity integer not null,
    primary key (basket_id,product_id)

);


create table if not exists orders

(
    order_id integer not null primary key generated always as identity,
    basket_id integer not null references basket(basket_id),
    total_price integer not null,
    order_created_date timestamp not null default now()



);



create table if not exists orderLines

(
    order_id integer not null references orders(order_id),
    product_id text not null references product(product_name),
    quantity integer not null,
    product_price integer not null,
    primary key (order_id, product_id)

);

 insert into categories(categorie_name)
 values
     ('fruits'),
     ('voiture'),
     ('viande'),
     ('alcool');

INSERT INTO product(product_name, product_price, categorie_id, url_img)
VALUES ('audi', 15000, 2, 'https://zupimages.net/up/21/43/npi7.jpg'),
       ('poulet', 18, 3, 'https://zupimages.net/up/21/43/n71g.jpeg'),
       ('nouille', 3, 1, 'https://zupimages.net/up/21/43/x8mb.jpg'),
       ('rhum', 13, 4,'https://zupimages.net/up/21/43/bg3k.jpg'),
       ('fraise', 3, 1, 'https://zupimages.net/up/20/51/fhc6.jpg'),
       ('pêche', 5, 1, 'https://zupimages.net/up/21/43/mzma.jpg'),
       ('steak', 4, 3, 'https://zupimages.net/up/21/43/ljgk.jpg'),
       ('peugeot 205', 3500, 2,'https://zupimages.net/up/21/43/sl31.jpg'),
       ('vodka', 10, 4, 'https://zupimages.net/up/21/43/ybvf.jpg'),
       ('pomme', 1, 1,'https://zupimages.net/up/21/43/s3b1.jpg'),
       ('fiat punto', 2000,2, 'https://zupimages.net/up/21/43/hvqf.jpg'),
       ('tekila', 17, 4, 'https://zupimages.net/up/21/43/15v5.jpeg')
returning  *;




