package com.zenika.emarket.emarket.specification;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public class SpecificationUtils {

    public static <V, E> Specification<E> hasAttributeEquals(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.equal(getPath(root, attribute), value);
        }
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeGreaterThanOrEqualsTo(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(attribute), value);
        }
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeLessThanOrEqualsTo(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(attribute), value);
        }
    }

    private static <I, O> Path<O> getPath(Root<I> root, String attributePath) {
        Path<I> path = root;
        for (String part : attributePath.split("\\.")) {
            path = path.get(part);
        }
        return (Path<O>) path;
    }

}
