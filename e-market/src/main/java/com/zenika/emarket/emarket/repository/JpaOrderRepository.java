package com.zenika.emarket.emarket.repository;

import com.zenika.emarket.emarket.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaOrderRepository extends JpaRepository<Order, Integer> {
}
