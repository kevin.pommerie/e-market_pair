package com.zenika.emarket.emarket.controller;


import com.zenika.emarket.emarket.domain.Categorie;
import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.dto.*;
import com.zenika.emarket.emarket.service.CategoriesService;
import com.zenika.emarket.emarket.service.ProductService;
import com.zenika.emarket.emarket.specification.ProductSpecifications;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
@Slf4j
public class CategoriesController {

    private CategoriesService categoriesService;
    private ProductService productService;


    public CategoriesController(CategoriesService categoriesService, ProductService productService) {
        this.categoriesService = categoriesService;
        this.productService = productService;


        LOGGER.info("Categories Controller initialized");
    }

    @GetMapping
    public PageDto<CategorieLightDto> getAllCategories(@SortDefault("categorieId") Pageable pageable) {

        return PageDto.fromDomain(categoriesService.getAllCategories(pageable), CategorieLightDto::fromDomain );
    }

    @GetMapping("/{id}")
    public CategorieFullDto getCategorieById(@PathVariable("id") int id) {

        return CategorieFullDto.fromDomain(categoriesService.getCategorieById(id));
    }

    @PostMapping
    public MessageDto addCategorie(@RequestBody Categorie categorie) {

        categoriesService.addCategorie(categorie);

        return new MessageDto(categorie.getName() + " has been added");
    }

    @DeleteMapping("/{id}")
    public MessageDto deleteCategorie(@PathVariable("id") int id) {

        categoriesService.deleteCategorie(id);

        return new MessageDto("categorie number " + id + " has been deleted");
    }

    @GetMapping("/{id}/products")
    PageDto<ProductDto> getProductFromCategorie(
            @SortDefault("productName") Pageable pageable,
            @PathVariable("id") int id,
            ProductFilterDto productFilterDto) {

        Specification<Product> productSpecification = ProductSpecifications.fromFilter(productFilterDto);

        return PageDto.fromDomain(productService.getProductFromCategorie(productSpecification,id, pageable),
                ProductDto::getDtoFromDomain);

    }

}
