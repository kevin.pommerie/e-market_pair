package com.zenika.emarket.emarket.specification;

import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.dto.ProductFilterDto;
import org.springframework.data.jpa.domain.Specification;

import static com.zenika.emarket.emarket.specification.SpecificationUtils.*;


public class ProductSpecifications {


    public static Specification<Product> fromFilter(ProductFilterDto productFilterDto) {
        return hasPriceEquals(productFilterDto.getPrice())
                .and(hasPriceGreaterOrEqualsThan(productFilterDto.getPriceMin()))
                .and(hasPriceLoweOrEqualsThan(productFilterDto.getPriceMax()));

    }

    public static Specification<Product> hasCategory(Integer categoryId) {
        return hasAttributeEquals("categorie.categorieId", categoryId);
    }


    public static Specification<Product> hasPriceEquals(Double price) {
        return hasAttributeEquals("productPrice", price);
    }

    public static Specification<Product> hasPriceGreaterOrEqualsThan(Double price) {

        return hasAttributeGreaterThanOrEqualsTo("productPrice", price);

    }

    public static Specification<Product> hasPriceLoweOrEqualsThan(Double price) {

        return hasAttributeLessThanOrEqualsTo("productPrice", price);

    }
}