package com.zenika.emarket.emarket.error;

public class CategorieNullException extends RuntimeException{
    public CategorieNullException() {
    }

    public CategorieNullException(String message) {
        super(message);
    }

    public CategorieNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public CategorieNullException(Throwable cause) {
        super(cause);
    }

    public CategorieNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
