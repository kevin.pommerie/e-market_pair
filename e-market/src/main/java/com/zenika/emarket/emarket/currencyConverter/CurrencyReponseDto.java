package com.zenika.emarket.emarket.currencyConverter;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class CurrencyReponseDto {

    private String date;
    private Map<String, Double> currency = new HashMap<>();



    @JsonAnySetter
    public void setCurrency(String currency, Double rate) {
        this.currency.put(currency, rate);
    }
}
