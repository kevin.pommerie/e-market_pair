package com.zenika.emarket.emarket.error;

public class CategorieNotFoundException extends RuntimeException{
    public CategorieNotFoundException() {
    }

    public CategorieNotFoundException(String message) {
        super(message);
    }

    public CategorieNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CategorieNotFoundException(Throwable cause) {
        super(cause);
    }

    public CategorieNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
