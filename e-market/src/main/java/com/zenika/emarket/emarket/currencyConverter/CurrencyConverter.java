package com.zenika.emarket.emarket.currencyConverter;

import com.zenika.emarket.emarket.domain.Product;

public interface CurrencyConverter {

    Product changeProductCurrency(Product product, String currency);
}
