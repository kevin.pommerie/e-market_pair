package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.ProductLine;
import lombok.Data;

@Data
public class ProductLineDto {

    private ProductDto productDto;
    private Integer quantity;


    public static ProductLineDto fromDomain(ProductLine productLine) {

        ProductLineDto productLineDto = new ProductLineDto();

        productLineDto.setProductDto(ProductDto.getDtoFromDomain(productLine.getProduct()));
        productLineDto.setQuantity(productLine.getQuantity());

        return productLineDto;
    }


}
