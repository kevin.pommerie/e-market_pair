package com.zenika.emarket.emarket.service;


import com.zenika.emarket.emarket.domain.Basket;
import com.zenika.emarket.emarket.domain.Order;
import com.zenika.emarket.emarket.domain.OrderFactory;
import com.zenika.emarket.emarket.error.BasketNotFoundException;
import com.zenika.emarket.emarket.error.OrderNotFoundException;
import com.zenika.emarket.emarket.error.ProductNotFoundException;
import com.zenika.emarket.emarket.repository.JpaBasketRepository;
import com.zenika.emarket.emarket.repository.JpaOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Slf4j
public class OrderService {

    private final JpaOrderRepository jpaOrderRepository;
    private final JpaBasketRepository jpaBasketRepository;

    public OrderService(JpaOrderRepository jpaOrderRepository, JpaBasketRepository jpaBasketRepository) {
        this.jpaOrderRepository = jpaOrderRepository;
        this.jpaBasketRepository = jpaBasketRepository;

        LOGGER.info("Order Service has been initialized");
    }


    @Transactional
    public List<Order> getAllOrders() {
        return jpaOrderRepository.findAll();
    }

    @Transactional
    public Order getOrderById(int orderId) {

        return jpaOrderRepository.findById(orderId)
                .orElseThrow(() -> new OrderNotFoundException(" Order does not exist"));
    }

    @Transactional
    public Order createNewOrder(int basketId) {

        Basket basket = jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" basket does not exist"));


            Order createdOrder = jpaOrderRepository.save(OrderFactory.createOrder(basket));
            basket.clear();
            return createdOrder;



    }


}
