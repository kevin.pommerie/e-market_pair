package com.zenika.emarket.emarket.repository;

import com.zenika.emarket.emarket.domain.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface JpaBasketRepository extends JpaRepository <Basket, Integer> {



    // use Optional<Basket> pour obliger la methode à prendre un .OrElseThrow

}
