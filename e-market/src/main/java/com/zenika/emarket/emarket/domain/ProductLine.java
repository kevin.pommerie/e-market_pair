package com.zenika.emarket.emarket.domain;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Embeddable
public class ProductLine {

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_name")
    private Product product;
    private int quantity;

    public ProductLine(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ProductLine() {

    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity){this.quantity = quantity;}
}
