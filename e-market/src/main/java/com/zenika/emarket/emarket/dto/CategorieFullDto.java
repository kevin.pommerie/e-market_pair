package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.Categorie;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
public class CategorieFullDto {

    private Integer id;
    private String name;
    private List<ProductDto> products = new ArrayList<>();


    public static CategorieFullDto fromDomain (Categorie categorie){

        CategorieFullDto categorieFullDto = new CategorieFullDto();

         categorieFullDto.setId(categorie.getCategorieId());
         categorieFullDto.setName((categorie.getName()));
         categorieFullDto.setProducts(categorie.getProducts().stream()
                 .map(ProductDto::getDtoFromDomain)
                 .collect(toList()));
         return categorieFullDto;
    }

}
