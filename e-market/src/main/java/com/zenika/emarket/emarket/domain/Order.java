package com.zenika.emarket.emarket.domain;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name= "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private int id;
    @ManyToOne
    @JoinColumn(name = "basket_id", referencedColumnName = "basket_id")
    private Basket basket;
    @Column(name = "total_price")
    private double totalPrice;
    @Column(name = "order_created_date")
    private Instant createdDate;


    @ElementCollection
    @CollectionTable(name = "orderlines",
           joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "order_id"))
    List<OrderLine> orderLines = new ArrayList<>();

    public Order() {
    }

    public Basket getBasket() {
        return basket;
    }

    public Order setBasket(Basket basket) {
        this.basket = basket;
        return this;
    }

    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }


    public double getTotalPrice() {
        return totalPrice;
    }

    public Order setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Order setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public Order setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
        return this;
    }

    // used for tests
    public OrderLine getOrderLineFromProduct(Product product) {


        for (int i = 0; i < orderLines.size(); i++) {

            Product productTested = orderLines.get(i).getProduct();

            if (productTested.getProductName().equals(product.getProductName())) {
                return orderLines.get(i);

            }
        }
        return null;

    }
}
