package com.zenika.emarket.emarket.dto;


import com.zenika.emarket.emarket.domain.Categorie;
import lombok.Data;

@Data
public class CategorieLightDto {


    private Integer id;
    private String name;

    public static CategorieLightDto fromDomain (Categorie categorie) {

        CategorieLightDto categorieLightDto = new CategorieLightDto();

        categorieLightDto.setId(categorie.getCategorieId());
        categorieLightDto.setName((categorie.getName()));

        return categorieLightDto;
    }
}
