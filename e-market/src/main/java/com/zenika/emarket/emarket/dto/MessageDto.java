package com.zenika.emarket.emarket.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class MessageDto {

    private final String message;
    private final Instant timestamp = Instant.now();


   }
