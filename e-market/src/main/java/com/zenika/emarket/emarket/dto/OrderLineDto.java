package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.OrderLine;
import lombok.Data;

@Data
public class OrderLineDto {

    private ProductDto productDto;
    private Integer Quantity;
    private Integer ProductPrice;



    public static OrderLineDto fromDomain(OrderLine orderLine) {

        OrderLineDto orderLineDto = new OrderLineDto();

        orderLineDto.setProductDto(ProductDto.getDtoFromDomain(orderLine.getProduct()));
        orderLineDto.setQuantity(orderLine.getQuantity());
        orderLineDto.setProductPrice(orderLineDto.getProductPrice());


        return orderLineDto;
    }
}
