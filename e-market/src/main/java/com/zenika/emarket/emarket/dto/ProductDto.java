package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.Product;
import lombok.Data;



@Data
public class ProductDto {

    private String id;
    private Double price;
    private String categorie;
    private String urlImg;

    public static ProductDto getDtoFromDomain(Product product){

        ProductDto productDto = new ProductDto();
        productDto.setId(product.getProductName());
        productDto.setPrice(product.getProductPrice());
        productDto.setCategorie(product.getCategorie().getName());
        productDto.setUrlImg(product.getUrlImg());
        return  productDto;
    }



}
