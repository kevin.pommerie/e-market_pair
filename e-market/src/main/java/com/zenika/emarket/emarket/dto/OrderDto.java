package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.Order;
import com.zenika.emarket.emarket.domain.OrderLine;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class OrderDto {

    private Integer id;
    private Integer basketId;
    private Double totalPrice;
    private Instant createdDate;

    List<OrderLineDto> orderLines = new ArrayList<>();

    public static OrderDto fromDomain(Order order) {

        OrderDto orderDto = new OrderDto();

        orderDto.setId(order.getId());
        orderDto.setBasketId(order.getBasket().getId());
        orderDto.setTotalPrice(order.getTotalPrice());
        orderDto.setCreatedDate(order.getCreatedDate());

        List<OrderLineDto> orderLineDtos = new ArrayList<>();

        for (OrderLine orderLine : order.getOrderLines()) {

            orderLineDtos.add(OrderLineDto.fromDomain(orderLine));
        }

        orderDto.setOrderLines(orderLineDtos);

        return orderDto;
    }
}
