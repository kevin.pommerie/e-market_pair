package com.zenika.emarket.emarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication(scanBasePackages ={"com.zenika"})
@ConfigurationPropertiesScan
public class EMarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(EMarketApplication.class, args);



    }

}
