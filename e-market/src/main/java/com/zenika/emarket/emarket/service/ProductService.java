package com.zenika.emarket.emarket.service;

import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.currencyConverter.CurrencyConverter;
import com.zenika.emarket.emarket.dto.ProductDto;
import com.zenika.emarket.emarket.error.ProductNotFoundException;
import com.zenika.emarket.emarket.repository.JpaProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.zenika.emarket.emarket.specification.ProductSpecifications.hasCategory;


@Service
@Slf4j
public class ProductService {

    private final JpaProductRepository jpaProductRepository;
    private final CurrencyConverter currencyConverter;

    public ProductService(JpaProductRepository jpaProductRepository, CurrencyConverter currencyConverter) {
        this.jpaProductRepository = jpaProductRepository;
        this.currencyConverter = currencyConverter;

        LOGGER.info("Product service has been initialized");
    }

    @Transactional
    public Page<Product> getAllProducts(Specification<Product> productSpecification, Pageable pageable, String currency) {


        return jpaProductRepository.findAll(productSpecification, pageable)
                .map(product -> convertProductCurrency(product, currency));

    }

    private Product convertProductCurrency(Product product, String currency) {

        if (currency.equals("eur")) {
            return product;
        } else {
            return currencyConverter.changeProductCurrency(product, currency);
        }

    }


    @Transactional
    public void deleteProduct(String productId) {

        Product product = jpaProductRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(" Product does not exist"));

        jpaProductRepository.delete(product);

    }

    @Transactional
    public void createProduct(Product product) {

        jpaProductRepository.save(product);

    }

    @Transactional
    public void updateProductPrice(String productName, Product product) {


        Product existingProduct = jpaProductRepository.findById(productName)
                .orElseThrow(() -> new ProductNotFoundException("product does not exist"));


        existingProduct.setProductPrice(product.getProductPrice());
        jpaProductRepository.save(existingProduct);


    }


    @Transactional
    public void updateUrlImg(String productName, Product product) {

        Product existingProduct = jpaProductRepository.findById(productName)
                .orElseThrow(() -> new ProductNotFoundException("product does not exist"));

        existingProduct.setUrlImg(product.getUrlImg());
        jpaProductRepository.save(existingProduct);

    }

    @Transactional
    public ProductDto getProductByName(String productName, String currency) {
        return ProductDto.getDtoFromDomain(jpaProductRepository.findById(productName)
                .map(product -> convertProductCurrency(product, currency))
                .orElseThrow(() -> new ProductNotFoundException("product does not exist")));
    }

    @Transactional
    public Page<Product> getProductFromCategorie(Specification<Product> productSpecification, Integer categoryId, Pageable pageable) {
        return jpaProductRepository.findAll(productSpecification.and(hasCategory(categoryId)), pageable);
    }
}
