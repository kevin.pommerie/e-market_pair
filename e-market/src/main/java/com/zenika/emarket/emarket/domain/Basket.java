package com.zenika.emarket.emarket.domain;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "basket")
public class Basket {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "basket_id")
    private int id;
    @Column(name = "created_at")
    private Instant createdAt;

    @ElementCollection
    @CollectionTable(name = "productline",  //colonne dans productline ------- colonne de ref dans basket
            joinColumns = @JoinColumn(name = "basket_id", referencedColumnName = "basket_id"))
    List<ProductLine> productLines = new ArrayList<>();


    public Basket() {
    }

    public List<ProductLine> getProductLines() {
        return productLines;
    }

    public void setProductLines(List<ProductLine> productLines) {
        this.productLines = productLines;
    }

    public ProductLine getProductLineFromProduct(Product product) {


        for (int i = 0; i < productLines.size(); i++) {

            Product productTested = productLines.get(i).getProduct();

            if (productTested.getProductName().equals(product.getProductName())) {
                return productLines.get(i);

            }
        }
        return null;

    }

    public void addProduct(Product product, int quantity) {


        ProductLine productLine = getProductLineFromProduct(product);

        if (productLine != null) {
            productLine.setQuantity(productLine.getQuantity() + quantity);}

        else {
            ProductLine productLineNew = new ProductLine(product, quantity);
            productLines.add(productLineNew);

        }


    }


    public void removeProduct(Product product, int quantity) {

        ProductLine productLine = getProductLineFromProduct(product);
        int updatedQuantity = productLine.getQuantity();
        updatedQuantity -= quantity;

        if (updatedQuantity <= 0) {
            productLines.remove(productLine);
        } else {productLine.setQuantity(updatedQuantity);}


    }

    public void clear() {
        productLines.clear();
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }


    public void setId(int id) {

        this.id = id;
    }

    public int getId() {

        return id;
    }
}
