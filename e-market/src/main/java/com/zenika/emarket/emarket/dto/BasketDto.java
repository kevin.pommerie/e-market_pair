package com.zenika.emarket.emarket.dto;

import com.zenika.emarket.emarket.domain.Basket;
import com.zenika.emarket.emarket.domain.ProductLine;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Data
public class BasketDto {

    private Integer id;
    private Instant createdAt;
    private List<ProductLineDto> productLines = new ArrayList<>();


    public static BasketDto fromDomain(Basket basket) {

        BasketDto basketDto = new BasketDto();

        basketDto.setId(basket.getId());
        basketDto.setCreatedAt(basket.getCreatedAt());

        List<ProductLineDto> productLineDtos = new ArrayList<>();

        for (ProductLine productLine : basket.getProductLines()) {

            productLineDtos.add(ProductLineDto.fromDomain(productLine));
        }

        basketDto.setProductLines(productLineDtos);

        return basketDto;
    }


}
