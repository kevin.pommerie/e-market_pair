package com.zenika.emarket.emarket.repository;

import com.zenika.emarket.emarket.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository                                // Jparepo <object, type of ID>
public interface JpaProductRepository extends JpaRepository<Product, String>, JpaSpecificationExecutor<Product> {







}
