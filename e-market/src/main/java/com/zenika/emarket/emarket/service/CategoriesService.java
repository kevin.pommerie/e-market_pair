package com.zenika.emarket.emarket.service;


import com.zenika.emarket.emarket.domain.Categorie;
import com.zenika.emarket.emarket.error.CategorieNotFoundException;
import com.zenika.emarket.emarket.repository.JpaCategoriesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
public class CategoriesService {

    private final JpaCategoriesRepository jpaCategoriesRepository;

    public CategoriesService(JpaCategoriesRepository jpaCategoriesRepository) {
        this.jpaCategoriesRepository = jpaCategoriesRepository;

        LOGGER.info("Categories Service initialized");
    }

    @Transactional
    public Page<Categorie> getAllCategories(Pageable pageable) {

        return jpaCategoriesRepository.findAll(pageable);
    }

    @Transactional
    public Categorie getCategorieById(int id) {
        return jpaCategoriesRepository.findById(id)
                .orElseThrow(() -> new CategorieNotFoundException("Categorie not found"));
    }

    @Transactional
    public void addCategorie(Categorie categorie) {

        jpaCategoriesRepository.save(categorie);

    }

    @Transactional
    public void deleteCategorie(int categorieId) {

        Categorie categorieToDelete = jpaCategoriesRepository.findById(categorieId)
                .orElseThrow(() -> new CategorieNotFoundException("Categorie not found"));

        jpaCategoriesRepository.delete(categorieToDelete);

    }


}
