package com.zenika.emarket.emarket.domain;


import com.zenika.emarket.emarket.error.CategorieNullException;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categories")
public class Categorie {


    @Id
    @Column(name = "categorie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer categorieId;


    @Column(name = "categorie_name")
    private String name;

    @OneToMany(mappedBy = "categorie")
    private List<Product> products;

    public Categorie(Integer id, String name) {
        this.categorieId = id;
        this.name = name;

        if (name == null) throw new CategorieNullException("vous devez spécifier un nom");
    }

    public Categorie() {

    }

    public Integer getCategorieId() {
        return categorieId;
    }


    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
        return products;
    }
}
