package com.zenika.emarket.emarket.domain;

import javax.persistence.*;

@Embeddable
public class OrderLine {

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_name")
    private Product product;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "product_price")
    private double productPrice;

    public OrderLine() {

    }

    public OrderLine(Product product, int quantity, double productPrice) {
        this.product = product;
        this.quantity = quantity;
        this.productPrice = productPrice;

    }

    @ManyToOne
    public Product getProduct() {
        return product;
    }

    public OrderLine setProduct(Product product) {
        this.product = product;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public OrderLine setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public OrderLine setProductPrice(double productPrice) {
        this.productPrice = productPrice;
        return this;
    }


}
