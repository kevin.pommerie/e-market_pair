package com.zenika.emarket.emarket.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ErrorControllerAdvice {

    public ErrorControllerAdvice() {
        LOGGER.info("ErrorControllerAdvice has been initialized");
    }

    @ExceptionHandler(ProductNullException.class)
    public ResponseEntity<HttpError> onProductException(ProductNullException e) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new HttpError("Can't be Null " + e.getProductName()));
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<HttpError> onProductNotFoundException(ProductNotFoundException e){
        LOGGER.warn("hoooo dear, we are in trouble", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("Product not found !" + e.getMessage()));
    }

    @ExceptionHandler(BasketNotFoundException.class)
    public ResponseEntity<HttpError> onBasketNotFoundException(BasketNotFoundException e){
        LOGGER.warn("c'est chelou", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("Basket not found !" + e.getMessage()));
    }

    @ExceptionHandler(CategorieNotFoundException.class)
    public ResponseEntity<HttpError> onCategorieNotFoundException(CategorieNotFoundException e){
        LOGGER.warn("weird !", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("Categorie not found !" + e.getMessage()));
    }

    @ExceptionHandler(CategorieNullException.class)
    public ResponseEntity<HttpError> onCathegorieNullException(CategorieNullException e){
        LOGGER.warn("damn !", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("Can't be null" + e.getMessage()));
    }

    @ExceptionHandler(CurrencyNotFoundException.class)
    public ResponseEntity<HttpError> onCurrencyNotFoundException(CurrencyNotFoundException e){
        LOGGER.warn("oopsy", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("currency does not exist" + e.getMessage()));
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<HttpError> onOrderNotFoundException(CurrencyNotFoundException e){
        LOGGER.warn("choubidouwaaaa", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("order does not exist" + e.getMessage()));
    }
}
