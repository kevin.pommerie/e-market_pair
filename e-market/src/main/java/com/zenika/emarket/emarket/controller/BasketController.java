package com.zenika.emarket.emarket.controller;

import com.zenika.emarket.emarket.domain.Basket;
import com.zenika.emarket.emarket.domain.Order;
import com.zenika.emarket.emarket.dto.BasketDto;
import com.zenika.emarket.emarket.dto.MessageDto;
import com.zenika.emarket.emarket.dto.OrderDto;
import com.zenika.emarket.emarket.service.BasketService;
import com.zenika.emarket.emarket.service.OrderService;
import com.zenika.emarket.emarket.service.ProductLineService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController()
@CrossOrigin
@Slf4j
public class BasketController {


    private final ProductLineService productLineService;
    private final BasketService basketService;
    private final OrderService orderService;


    public BasketController(ProductLineService productLineService, BasketService basketService, OrderService orderService) {
        this.productLineService = productLineService;
        this.basketService = basketService;
        this.orderService = orderService;

        LOGGER.info("Basket Controller has been initialized");

    }

    @PostMapping("/basket")
    public Basket addNewBasket() {

        return basketService.addNewBasket();
    }


    @GetMapping("/basket")
    public List<Integer> getAllBaskets() {

        return basketService.getAllBaskets().stream()
                .map(basket -> basket.getId())
                .collect(Collectors.toList());
    }


    @GetMapping("/basket/{id}")
    public BasketDto getBasketById(@PathVariable("id") int id) {


        return BasketDto.fromDomain(basketService.getBasketById(id));


    }

    @DeleteMapping("/basket/{id}")
    public MessageDto deleteBasket(@PathVariable("id") int id) {

        basketService.deleteBasket(id);
        return new MessageDto( "basket number " + id + " has been deleted ");

    }


    @PostMapping("/basket/{id}/products/{productId}")
    public MessageDto addProductToBasket(
            @PathVariable("productId") String pId,
            @PathVariable("id") int id,
            @RequestBody QuantityDTO quantityDTO) {

        return new MessageDto(productLineService.addProductToBasket(pId, id, quantityDTO.getQuantity()));


    }


    @DeleteMapping("/basket/{id}/products/{product}")
    public MessageDto deleteProductFromBasket
            (@PathVariable("id") int id,
             @PathVariable("product") String name,
             @RequestBody QuantityDTO quantityDTO) {

        return new MessageDto(productLineService.deleteProductFromBasket(name, id, quantityDTO.getQuantity()));
    }


    @DeleteMapping("basket/{id}/products")
    public MessageDto clearBasket(@PathVariable ("id") int id){

        return new MessageDto(productLineService.clearBasket(id));
    }

    @PostMapping("/basket/{id}/order")
    public OrderDto buyBasket(@PathVariable ("id") int id){

        Order order = orderService.createNewOrder(id);

        return OrderDto.fromDomain(order);

    }
}

@Data
class QuantityDTO {

    private int quantity;

}