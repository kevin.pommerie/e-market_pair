package com.zenika.emarket.emarket.dto;

import lombok.Data;

@Data
public class ProductFilterDto {

    private String id;
    private Double price;
    private Double priceMin;
    private Double priceMax;
    private String categorie;
    private String urlImg;





}
