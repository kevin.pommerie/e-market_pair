package com.zenika.emarket.emarket.domain;

import com.zenika.emarket.emarket.error.CategorieNullException;
import com.zenika.emarket.emarket.error.ProductNullException;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "product_name")
    private String productName;

    public Product(String productName, double productPrice) {
        this.productName = productName;
        this.productPrice = productPrice;
    }

    @Column(name = "product_price")
    private double productPrice;

    @ManyToOne
    @JoinColumn(name = "categorie_id")
    private Categorie categorie;

    @Column(name = "url_img")
    private String urlImg;


    public Product(String productName, double productPrice, Categorie categorieId, String urlImg) {
        this.productName = productName;
        this.productPrice = Math.round(productPrice * 100d) / 100d;
        this.categorie = categorieId;
        this.urlImg = urlImg;

        if (productName == null) {
            throw new ProductNullException("vous devez spécifier un nom");
        }

        if (productPrice <= 0) {
            throw new ProductNullException("le prix ne peux pas être inférieur ou égal à zero", this.productName);
        }

        if (categorieId == null) {
            throw new CategorieNullException("spécifiez une categorie qui existe");
        }
    }

    public Product() {

    }




    public String getProductName() {

        return productName.toLowerCase();
    }

    public double getProductPrice() {
        return productPrice;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setProductPrice(double productPrice) {

        this.productPrice = productPrice;

        if (productPrice <= 0) {
            throw new ProductNullException("le prix ne peut pas être inférieur à zero", this.productName);
        }
    }


    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {

        return getProductName() + " ---> " + getProductPrice() + "€";
    }

    public String getUrlImg() {
        return urlImg;
    }

    public Product setUrlImg(String urlImg) {
        this.urlImg = urlImg;
        return this;
    }
}


