package com.zenika.emarket.emarket.repository;

import com.zenika.emarket.emarket.domain.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface JpaCategoriesRepository extends JpaRepository<Categorie, Integer> {
}
