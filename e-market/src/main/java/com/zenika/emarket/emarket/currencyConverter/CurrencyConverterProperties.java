package com.zenika.emarket.emarket.currencyConverter;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "price.currency.converter")
public class CurrencyConverterProperties {

    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public CurrencyConverterProperties setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }
}
