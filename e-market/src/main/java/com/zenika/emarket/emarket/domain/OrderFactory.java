package com.zenika.emarket.emarket.domain;

import com.zenika.emarket.emarket.error.ProductNotFoundException;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class OrderFactory {


    public static Order createOrder(Basket basket){

        if (basket.getProductLines().size() > 0){
        return convertToOrder(basket);
        } else{
            throw new ProductNotFoundException(" basket is empty");
        }
    }



    private OrderLine convertFromProductLine(ProductLine productLine){

        OrderLine orderLine = new OrderLine();

        orderLine.setProduct(productLine.getProduct());
        orderLine.setProductPrice(productLine.getProduct().getProductPrice());
        orderLine.setQuantity(productLine.getQuantity());

        return orderLine;
    }

    private Order convertToOrder(Basket basket){

        Order order = new Order();
        List<OrderLine> orderLineList = new ArrayList<>();
        double totalPrice = 0;

        for (ProductLine productLine : basket.getProductLines()){

            OrderLine orderLine = convertFromProductLine(productLine);
            orderLineList.add(orderLine);

            totalPrice += productLine.getProduct().getProductPrice() * productLine.getQuantity();
        }
        order.setOrderLines(orderLineList);
        order.setBasket(basket);
        order.setTotalPrice(totalPrice);
        order.setId(basket.getId());
        order.setCreatedDate(Instant.now());


        return order;
    }
}
