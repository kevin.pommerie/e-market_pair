package com.zenika.emarket.emarket.service;


import com.zenika.emarket.emarket.domain.Basket;
import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.error.BasketNotFoundException;
import com.zenika.emarket.emarket.error.ProductNotFoundException;
import com.zenika.emarket.emarket.repository.JpaBasketRepository;
import com.zenika.emarket.emarket.repository.JpaProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
public class ProductLineService {

    JpaBasketRepository jpaBasketRepository;
    JpaProductRepository jpaProductRepository;

    public ProductLineService(JpaBasketRepository jpaBasketRepository, JpaProductRepository jpaProductRepository) {
        this.jpaBasketRepository = jpaBasketRepository;
        this.jpaProductRepository = jpaProductRepository;

        LOGGER.info("ProductLine service has been initialized");
    }


    @Transactional
    public String addProductToBasket(String productId, int basketId, int quantity) {

        Product product = jpaProductRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(" Product does not exist"));

        Basket basket = jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" basket does not exist"));

        basket.addProduct(product, quantity);

        return product.getProductName() + " has been added to basket " + basket.getId();
    }

    @Transactional
    public String deleteProductFromBasket(String productId, int basketId, int quantity) {

        Product product = jpaProductRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(" Prooduct does not exist"));

        Basket basketTodelete = jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" Basket does not exist"));


        basketTodelete.removeProduct(product, quantity);

        return String.valueOf(quantity) + " " + product.getProductName() + " has been deleted from basket "
                + basketTodelete.getId();
    }

    @Transactional
    public String clearBasket( int basketId ){

        Basket basketToClear = jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" Basket does not exist"));

       basketToClear.clear();
       return "basket " + basketId + " has been cleared" ;
    }
}
