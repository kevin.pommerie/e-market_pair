package com.zenika.emarket.emarket.error;

public class ProductNullException extends RuntimeException {

    private final String productName;

    public String getProductName() {
        return productName;
    }

    public ProductNullException(String productName) {
        this.productName = productName;
    }

    public ProductNullException(String message, String productName) {
        super(message);
        this.productName = productName;
    }

    public ProductNullException(String message, Throwable cause, String productName) {
        super(message, cause);
        this.productName = productName;
    }

    public ProductNullException(Throwable cause, String productName) {
        super(cause);
        this.productName = productName;
    }

    public ProductNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String productName) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.productName = productName;
    }
}
