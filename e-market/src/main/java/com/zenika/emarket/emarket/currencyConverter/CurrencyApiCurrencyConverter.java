package com.zenika.emarket.emarket.currencyConverter;

import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.error.CurrencyNotFoundException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;



@Component
public class CurrencyApiCurrencyConverter implements CurrencyConverter {

    private final RestTemplate restTemplate;

    public CurrencyApiCurrencyConverter(RestTemplateBuilder restTemplateBuilder,
                                        CurrencyConverterProperties currencyConverterProperties) {

        this.restTemplate = restTemplateBuilder.
                rootUri(currencyConverterProperties.getBaseUrl()).build();


    }


    @Override
    public Product changeProductCurrency(Product product, String currency) {

        CurrencyReponseDto currencyResponseDto = restTemplate.getForObject("/eur/{currency}.json", CurrencyReponseDto.class, currency);

        if (currencyResponseDto != null && currencyResponseDto.getCurrency().get(currency) != null) {
            return new Product(
                    product.getProductName(),
                    product.getProductPrice() * currencyResponseDto.getCurrency().get(currency),
                    product.getCategorie(),
                    product.getUrlImg());
        } else {
            throw new CurrencyNotFoundException("currency does not exist");
        }

    }
}
