package com.zenika.emarket.emarket.dto;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class PageDto<T> {

    public static <I, O> PageDto<O> fromDomain(Page<I> page, Function<I, O> domainIntoDto){

// don't forget to specifie defaut page, max page size and else in properties
        return new PageDto<O>()
                .setContent(

                   page.getContent().stream()
                           .map(domainIntoDto)
                           .collect(toList()))
                .setPage(page.getPageable().getPageNumber() +1) // Page index start at 1 in REST API but at 0 in Spring Data
                .setSize(page.getPageable().getPageSize())
                .setCount(page.getNumberOfElements())
                .setTotalPage(page.getTotalPages())
                .setTotalCount(page.getTotalElements())

                ;

    }
    private List<T> content;
    private int page;
    private int size;
    private int count;
    private long totalCount;
    private int totalPage;

    public List<T> getContent() {
        return content;
    }

    public PageDto<T> setContent(List<T> content) {
        this.content = content;
        return this;
    }

    public int getPage() {
        return page;
    }

    public PageDto<T> setPage(int page) {
        this.page = page;
        return this;
    }

    public int getSize() {
        return size;
    }

    public PageDto<T> setSize(int size) {
        this.size = size;
        return this;
    }

    public int getCount() {
        return count;
    }

    public PageDto<T> setCount(int count) {
        this.count = count;
        return this;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public PageDto<T> setTotalCount(long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public PageDto<T> setTotalPage(int totalPage) {
        this.totalPage = totalPage;
        return this;
    }
}
