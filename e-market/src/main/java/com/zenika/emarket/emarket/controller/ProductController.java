package com.zenika.emarket.emarket.controller;

import com.zenika.emarket.emarket.domain.Product;
import com.zenika.emarket.emarket.dto.MessageDto;
import com.zenika.emarket.emarket.dto.PageDto;
import com.zenika.emarket.emarket.dto.ProductDto;
import com.zenika.emarket.emarket.dto.ProductFilterDto;
import com.zenika.emarket.emarket.service.ProductService;
import com.zenika.emarket.emarket.specification.ProductSpecifications;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/products")
@CrossOrigin
@Slf4j
public class ProductController {

    ProductService productService;


    public ProductController(ProductService productService) {

        this.productService = productService;


        LOGGER.info("Product controller has been initialized");


    }


    @GetMapping
    public PageDto<ProductDto> getProductList(@SortDefault(value = {"productName"}) Pageable pageable,
                                              ProductFilterDto productFilterDto,
                                              @RequestParam(required = false, defaultValue = "eur") String currency) {

        Specification<Product> productSpecification = ProductSpecifications.fromFilter(productFilterDto);

        return PageDto.fromDomain(productService.getAllProducts( productSpecification, pageable, currency ),
                ProductDto::getDtoFromDomain);
    }


    @DeleteMapping("/{id}")
    public MessageDto deleteProduct(@PathVariable("id") String name) {

        productService.deleteProduct(name);

        return new MessageDto(name + " has been deleted ");
    }

    @PostMapping
    public MessageDto createProduct(@RequestBody Product product) {

        productService.createProduct(product);

        return new MessageDto(product.getProductName() + " has been created, and cost "
                + product.getProductPrice());
    }

    @PutMapping("/{id}")
    public MessageDto updateProductPrice(@PathVariable("id") String name,
                                         @RequestBody Product product) {

        productService.updateProductPrice(name, product);

        return new MessageDto(name + "'s price, has been updated to " + product.getProductPrice());

    }

    @PutMapping("/{id}/img")
    public MessageDto updateProductImg(@PathVariable("id") String name,
                                        @RequestBody Product product){

        productService.updateUrlImg(name, product);
        return new MessageDto(name + "'s image has been updated to " + product.getUrlImg() );

    }

    @GetMapping("/{id}")
    public ProductDto getProductByName(@PathVariable("id") String name,
                                       @RequestParam(required = false, defaultValue = "eur") String currency) {
        return productService.getProductByName(name, currency);
    }





}
