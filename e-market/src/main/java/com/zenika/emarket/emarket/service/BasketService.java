package com.zenika.emarket.emarket.service;


import com.zenika.emarket.emarket.domain.Basket;
import com.zenika.emarket.emarket.error.BasketNotFoundException;
import com.zenika.emarket.emarket.repository.JpaBasketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
public class BasketService {

    private final JpaBasketRepository jpaBasketRepository;

    public BasketService(JpaBasketRepository jpaBasketRepository) {
        this.jpaBasketRepository = jpaBasketRepository;

        LOGGER.info("Basket service has been initialized");
    }

    @Transactional
    public Basket addNewBasket() {
        Basket basket = new Basket();
        basket.setCreatedAt(Instant.now());

        jpaBasketRepository.save(basket);

        return basket;
    }

    @Transactional
    public List<Basket> getAllBaskets() {
        return jpaBasketRepository.findAll();

    }

    @Transactional
    public Basket getBasketById(int basketId) {

        return jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" Basket does not exist"));
    }

    @Transactional
    public void deleteBasket(int basketId) {

        Basket basketToDelete = jpaBasketRepository.findById(basketId)
                .orElseThrow(() -> new BasketNotFoundException(" Basket does not exist"));

        jpaBasketRepository.delete(basketToDelete);

    }
}
