package com.zenika.emarket.emarket.error;

public class ProductNotFoundException extends RuntimeException{

    private final String productname;

    public ProductNotFoundException(String productname) {
        this.productname = productname;
    }

    public ProductNotFoundException(String message, String productname) {
        super(message);
        this.productname = productname;
    }

    public ProductNotFoundException(String message, Throwable cause, String productname) {
        super(message, cause);
        this.productname = productname;
    }

    public ProductNotFoundException(Throwable cause, String productname) {
        super(cause);
        this.productname = productname;
    }

    public ProductNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String productname) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.productname = productname;
    }
}
